﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace MesozoicSolution
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dinosaur dinosaur = new Stegausaurus("Louis", 12);
            Stegausaurus dinosaur2 = new Stegausaurus("Louis", 12);
            Dinosaur dinosaur3 = new Diplodocus("Louis", 12);
            Console.WriteLine(dinosaur == dinosaur2); // True
            Console.WriteLine(dinosaur == dinosaur3); // False
            Console.ReadKey();
        }
    }
}
