﻿using System;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        public string name;
        public static string specie;
        public int age;

        protected virtual string Specie { get { return "Dinosaur"; } }

        public Dinosaur(string name,  int age)
        {
            this.name = name;
            this.age = age;
        }

        public virtual string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, Dinosaur.specie, this.age);
        }

        public virtual string roar()
        {
            return "Grrr";
        }

        public string getName()
        {
            return this.name;
        }

        public string getSpecie()
        {
            return Dinosaur.specie;
        }

        public int getAge()
        {
            return this.age;
        }

        public void setName()
        {
            Console.Write("Donner son nom : ");
            string nom = Console.ReadLine();
            this.name = nom;
        }

        public void setSpecie()
        {
            Console.Write("Donner son espece : ");
            string specie = Console.ReadLine();
            Dinosaur.specie = specie;
        }

        public void setAge()
        {
            int age;
            string age_string;
            do
            {
                Console.Write("Donner son age : ");
                age_string = Console.ReadLine();
            } while (!int.TryParse(age_string, out age));
            this.age = age;
        }

        public string hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}.", this.name, dinosaur.name);
        }

        public override string ToString()
        {
            string afficher;
            afficher = "Mezosoic.";
            afficher += this.Specie;
            afficher += " = {";
            afficher += "name : ";
            afficher += this.name;
            afficher += ", age : ";
            afficher += this.age;
            afficher += "}";
            return afficher;
        }

        public override bool Equals(object obj)
        {
            return obj is Dinosaur && this == (Dinosaur)obj;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.name.GetHashCode();
            hash ^= this.age.GetHashCode();
            hash ^= this.Specie.GetHashCode();
            return hash;
        }

        public static bool operator ==(Dinosaur dino1, Dinosaur dino2)
        {
            return dino1.name == dino2.name && dino1.Specie == dino2.Specie && dino1.age == dino2.age;
        }

        public static bool operator !=(Dinosaur dino1, Dinosaur dino2)
        {
            return !(dino1 == dino2);
        }
    }
}