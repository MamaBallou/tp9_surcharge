﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class TyrannosaurusRex : Dinosaur
    {
        static TyrannosaurusRex()
        {
            TyrannosaurusRex.specie = "TyrannosaurusRex";
        }
        protected override string Specie { get { return "TyrannosaurusRex"; } }
        public TyrannosaurusRex(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "Grooooooaaaaaarrrrr";
        }
        public override string sayHello()
        {
            return string.Format("Tremblez de peur devant {0} le {1} qui vous dévorera du haut de ses {2} ans.", this.name, TyrannosaurusRex.specie, this.age);
        }
    }
}
