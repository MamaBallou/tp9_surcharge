﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Diplodocus : Dinosaur
    {
        static Diplodocus()
        {
            Diplodocus.specie = "Diplodocus";
        }
        protected override string Specie { get { return "Diplodocus"; } }
        public Diplodocus(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "Bru";
        }
        public override string sayHello()
        {
            return string.Format("Je suis {0} un {1}, je viens d'avoir {2} ans.", this.name, Diplodocus.specie, this.age);
        }
    }
}
