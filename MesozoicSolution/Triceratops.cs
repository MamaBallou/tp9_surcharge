﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Triceratops : Dinosaur
    {
        static Triceratops()
        {
            Triceratops.specie = "Triceratops";
        }
        protected override string Specie { get { return "Triceratops"; } }
        public Triceratops(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "Zbraah";
        }
        public override string sayHello()
        {
            return string.Format("Je suis un {0} de {1} ans et je m'appelle {2}.", Triceratops.specie, this.age, this.name);
        }
    }
}
